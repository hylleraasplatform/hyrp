# import copy
# from scipy.optimize import fmin_bfgs
# import sys
import warnings
from dataclasses import dataclass, replace
from typing import Any, Dict, List, Optional, Tuple, Union

import numpy as np

# import quaternionic as qn
# from scipy import optimize, spatial
# from scipy.spatial.transform import Rotation as R


@dataclass
class RPMolecule:
    """Molecule class using for storing and exchanging information."""

    atoms: List
    coordinates: Union[List, np.array]
    properties: Dict


MoleculeList = List[RPMolecule]


class ReactionPath:
    """Create, optimize, plot reaction paths.

    Parameters
    ----------
        molecules : list or Molecule
            input molecules
        method : hylleraas method object, optional
            contains methods for computing energy and gradient
        options: dict
            optional options for optimizer

    """

    def __init__(self,
                 molecules: Union[MoleculeList, RPMolecule],
                 method: Optional[Any] = None,
                 options: Optional[Dict] = None):

        self.molecules = self._prepare_molecules(molecules)
        self.atoms = self.molecules[0].atoms
        self.base_properties = {
            key: val
            for key, val in self.molecules[0].properties.items()
            if key not in ['energy', 'gradient', 'hess_inv', 'hessian']
        }

        self.method = method
        self.options = self._set_default_options(options)

    def _prepare_molecules(self, molecules: Union[MoleculeList, RPMolecule]):
        """Prepare molecules for reaction path methods.

        Parameters
        ----------
        molecules: list or molecule
            molecule or list of molecule

        Returns
        -------
        list
            list of molecules in RPMolecule format

        """
        molecules = [molecules] if not isinstance(molecules, list) else molecules
        return [self._convert_molecule(mol) for mol in molecules]

    def _convert_molecule(self, molecule: Any):
        """Convert molecule into RPMolecule format.

        Parameters
        ----------
        molecule : Any
            molecule as dictionary or class instance

        Returns
        -------
        RPMolecule
            instance of RPMolecule dataclass

        """
        if isinstance(molecule, Dict):
            atoms = molecule.get('atoms', None)
            coordinates = molecule.get('coordinates', None)
            properties = molecule.get('properties', {})
            if None in [atoms, coordinates]:
                raise Exception(f'could not convert molecule {molecule}')
        else:
            try:
                if hasattr(molecule, 'atoms'):
                    atoms = molecule.atoms
                    coordinates = molecule.coordinates
                    properties = molecule.properties
                else:
                    atoms = molecule._atoms
                    coordinates = molecule._coordinates
                    properties = molecule._properties
            except Exception:
                raise Exception(f'could not convert molecule {molecule}')

        properties = {} if properties is None else properties

        return RPMolecule(atoms=atoms, coordinates=coordinates, properties=properties)

    def _set_default_options(self, options: Dict) -> Dict:
        """Set default options (dict) for path optimizer."""
        default: Dict = {'maxit': 30, 'maxrms': 1e-3, 'align': False}
        if options is not None:
            default.update(options)
        return default

    def plot(self,
             molecules: Optional[MoleculeList] = None,
             reference: Optional[int] = None,
             unit: Optional[str] = None):
        """Plot energy diagram.

        Parameters
        ----------
        molecules : list or molecule, optional
            (list of) molecules
        reference : int, optional
            index of molecule whos energy is used as zero, defaults to None
        unit : str, optional
            energy unit, defaults to kJ/mol

        Note
        ----
            the parameters molecule.properties['name'] and molecule.properties['energy'] are used for plotting
            see https://github.com/giacomomarchioro/PyEnergyDiagrams

        """
        unit = 'kJ/mol' if unit is None else unit
        import matplotlib.pyplot as plt
        from energydiagram import ED
        diagram = ED()
        if molecules is None:
            molecules = self.molecules

        # en_raw = [
        #     mol.properties.get('energy', None)
        #     for mol in molecules
        #    if mol.properties.get('energy', None) is not None
        # ]
        en_raw = [ mol.properties.get('energy', self.method.get_energy(mol)) for mol in molecules]

        if reference is None:
            e_ref = 0.0
        else:
            e_ref = en_raw[reference]

        if unit == 'kJ/mol':
            scale = 627.503
        elif unit == 'cm-1':
            scale = 219474.63
        elif unit == 'Hartree':
            scale = 1.0

        ylabel = f'Energy in {unit}'
        en = [scale * (ei-e_ref) for ei in en_raw]

        i = -1
        for mol in molecules:
            name = mol.properties.get('name', ' ')
            val = mol.properties.get('energy', None)
            if val is None:
                continue
            i += 1
            nscale = 2
            if unit == 'Hartree':
                nscale = 6
            val = round(en[i], nscale)
            diagram.add_level(val, name)
            if i > 0:
                diagram.add_link(i - 1, i)

        diagram.plot(ylabel=ylabel, show_IDs=False)
        plt.show()

    def estimate_tst(self):
        """Estimate structure of transition state among the molecules in the reaction path."""
        en: list = []
        for mol in self.molecules:
            eni = mol.properties.get('energy', None)
            if eni is None:
                try:
                    eni = self.method.get_energy(mol)
                except Exception:
                    raise Exception(f'could not find or compute energy in molecule {mol}')
            en.append(eni)

        n = len(en)

        if n % 2 == 1:
            enmax1_i = np.argmax(en)
            enmax1 = en.pop(enmax1_i)
            if enmax1_i == 0 or enmax1_i == n - 1:
                warnings.warn('estimated TST corresponds to maximum at the boundary')
            tst = self.molecules[enmax1_i]
        else:
            enmax1_i = np.argmax(en)
            enmax1 = en.pop(enmax1_i)
            enmax2_i = np.argmax(en)
            enmax2 = en.pop(enmax2_i)
            # check if we are at the left or right side
            if enmax1_i == 0 or enmax1_i == n - 1:
                tst = self.molecules(enmax1_i)
                warnings.warn('estimated TST corresponds to maximum at the boundary')
            else:
                k = enmax2 / (enmax1+enmax2)
                x1 = np.array(self.molecules[enmax1_i].coordinates).ravel()
                x2 = np.array(self.molecules[enmax2_i].coordinates).ravel()
                coord = (x1 + k * (x2-x1)).reshape(-1, 3)
                tst = replace(self.molecules[enmax1_i], coordinates=coord, properties=self.base_properties)

        try:
            en_tst = tst.properties.get('energy', self.method.get_energy(tst))
            if en_tst < enmax1:
                tst = self.molecules(enmax1_i)
                en_tst = enmax1
            grad_tst = tst.properties.get('gradient', self.method.get_gradient(tst))
            grad_norm = np.linalg.norm(grad_tst.ravel())
            if grad_norm > self.options['maxrms']:
                warnings.warn(f'non-zero gradient norm of estimated TST  ({grad_norm})')
            prop = self.base_properties.copy()
            prop.update({'energy': en_tst, 'gradient': grad_tst, 'name': 'TST'})
            tst = replace(tst, properties=prop)
        except Exception:
            warnings.warn('could not verify TST energy and gradient')

        return tst

    def align_molecules(self) -> MoleculeList:
        """Align molecules by minimize distance byrotation and translation, first molecule is fixed."""
        aligned_molecules: list = [self.molecules[0]]
        for i in range(1, len(self.molecules)):
            mol_aligned, _ = self._align_molecules_ab(self.molecules[i], self.molecules[i - 1])
            aligned_molecules.append(mol_aligned)
        return aligned_molecules

    def _align_molecules_ab(self, a, b) -> Tuple[RPMolecule, RPMolecule]:
        """Align molecule a relative to b.

        Parameters
        ----------
        a : RPMolecule
            molecule to be aligned

        b: RPMolecule
            reference

        Returns
        -------
        Tuple[RPMolecule, RPMolecule]
            aligned a and b

        """
        if not a.atoms == b.atoms:
            raise Exception('molecules atoms do not match')
        newcoord_a, _ = ReactionPath.align_vectors(a.coordinates, b.coordinates)
        a_new = replace(a, coordinates=newcoord_a, properties=self.base_properties)
        return a_new, b

    @classmethod
    def align_vectors(cls, vec1, vec2):
        """Align vector 1 relative to vector 2 by minimizing distance using rotations and translations."""
        vec1 = np.array(vec1)
        vec2 = np.array(vec2)
        if not len(vec1) == len(vec2):
            raise Exception('can only align vectors with same length')
        # formulae taken from https://igl.ethz.ch/projects/ARAP/svd_rot.pdf
        pc = sum(vec1) / len(vec1)
        qc = sum(vec2) / len(vec2)
        x = np.array([vec - pc for vec in vec1])
        y = np.array([vec - qc for vec in vec2])
        smat = x.T @ y
        u, s, vh = np.linalg.svd(smat)
        corr = np.identity(len(s))
        corr[-1, -1] = np.linalg.det(u @ vh)
        rot = u @ (corr@vh)
        t = qc - rot@pc
        vec3 = np.array([rot@vec + t for vec in vec2])
        dist_before = np.linalg.norm(vec1 - vec2)
        dist_after = np.linalg.norm(vec3 - vec2)
        if dist_after > dist_before:
            raise Exception(f'alignment fail {dist_before} -> {dist_after}')
        return vec3, vec2

    def interpolate(self, num: int) -> MoleculeList:
        """Generate intermediates by linear interpolation in the domains defined by self.molecules."""
        if len(self.molecules) < 2:
            return self.molecules
        if num <= len(self.molecules):
            return self.molecules

        num_domains = len(self.molecules) - 1
        n_per_domain = (num - len(self.molecules)) // num_domains

        molecules = []
        for i in range(num_domains):
            coord_start = self.molecules[i].coordinates.ravel()
            coord_end = self.molecules[i + 1].coordinates.ravel()
            coords = np.linspace(start=coord_start, stop=coord_end, endpoint=False, num=n_per_domain + 1)
            for coord in coords:
                mol = RPMolecule(atoms=self.atoms,
                                 coordinates=coord.reshape(-1, 3),
                                 properties=self.base_properties)
                molecules.append(mol)
        molecules.append(self.molecules[-1])

        return molecules

    # def initiate_molecules(self, mols):
    #     """Initiate molecules by computing energy, gradient, inital hessian approximation."""
    #     molecules = []
    #     for i, mol in enumerate(mols):
    #         en = self.method.get_energy(mol)
    #         grad = self.method.get_gradient(mol)
    #         hess_inv = np.identity(3 * len(self.atoms))
    #         prop = self.base_properties.copy()
    #         prop.update({'energy': en, 'gradient': grad, 'hess_inv': hess_inv})
    #         molecules.append(replace(mol, properties=prop))

    #     return molecules

    def update_molecules_hessian(self, mols):
        """Update hessians of molecules."""
        molecules = []
        for j, mol in enumerate(mols):
            molecules.append(self.update_hessian(mol))
        return molecules

    def update_hessian(self, molecule: RPMolecule):
        """Update inverse of hessian using BFGS update."""
        ek = molecule.properties.get('energy', self.method.get_energy(molecule))
        xk = np.array(molecule.coordinates).ravel()
        gk = molecule.properties.get('gradient', self.method.get_gradient(molecule)).ravel()
        bk = molecule.properties.get('hess_inv', np.identity(len(gk)))

        pk = -(bk @ gk)
        mk = pk @ gk

        wolfe1 = False
        wolfe2 = False
        ak = 1.0
        # primitive backtrace linesearch
        for i in range(5):
            sk = ak * pk
            xl = xk + sk
            mol = replace(molecule, coordinates=xl.reshape(-1, 3))
            el = self.method.get_energy(mol)
            gl = self.method.get_gradient(mol).ravel()
            c1 = 0.9
            c2 = 0.5
            wolfe1 = (el <= ek + c1*ak*mk)
            wolfe2 = (-(pk @ gl) <= -c2 * mk)
            if all([wolfe1, wolfe2]):
                break
            ak *= 0.7

        yk = gl - gk
        sy = sk @ yk
        bfgs1 = (yk @ (bk@yk) / sy + 1.0) * np.outer(sk, sk) / sy
        bfgs2 = np.outer(bk @ yk, sk) / sy + np.outer(sk, yk) @ bk / sy
        hess_inv = bk + bfgs1 - bfgs2

        prop = self.base_properties.copy()
        prop.update({'energy': el, 'gradient': gl, 'hess_inv': hess_inv})

        return replace(mol, properties=prop)

    def get_tangents(self):
        """Compute tangents to reaction path."""
        v = [mol.properties.get('energy', self.method.get_energy(mol)) for mol in self.molecules]
        r = [mol.coordinates.ravel() for mol in self.molecules]
        n = len(self.molecules)
        tangents = np.zeros_like(r)
        tangents[0] = np.zeros_like(r[0])
        tangents[-1] = np.zeros_like(r[-1])

        for i in range(1, n - 1):
            tau_plus = r[i + 1] - r[i]
            tau_minus = r[i] - r[i - 1]
            if v[i + 1] > v[i] > v[i - 1]:
                tangents[i] = tau_plus
            elif v[i + 1] < v[i] < v[i - 1]:
                tangents[i] = tau_minus
            else:
                deltavmax = max(abs(v[i + 1] - v[i]), abs(v[i - 1] - v[i]))
                deltavmin = min(abs(v[i + 1] - v[i]), abs(v[i - 1] - v[i]))

                if v[i + 1] > v[i - 1]:
                    tangents[i] = tau_plus*deltavmax + tau_minus*deltavmin
                else:
                    tangents[i] = tau_plus*deltavmin + tau_minus*deltavmax

            tangents[i] /= np.linalg.norm(tangents[i])
        return tangents

    def _optimize_distances(self, molecules, t):
        """Optimize expansion coefficients to asure equidistant points."""
        x = [mol.coordinates.ravel() for mol in molecules]
        g = [mol.properties.get('gradient', None).ravel() for mol in molecules]
        hess_inv = [mol.properties.get('hess_inv', None) for mol in molecules]
        n = len(molecules)
        # initiate
        a = np.zeros(n)
        for i in range(1, n - 1):
            a[i] = t[i] @ (hess_inv[i] @ g[i]) / (t[i] @ (hess_inv[i] @ t[i]))

        delta_x = np.zeros_like(x)
        ti = np.zeros_like(t)
        ti_minus = np.zeros_like(t)
        qi = np.zeros_like(t)
        for i in range(1, n):
            qi[i] = x[i] - x[i - 1] - hess_inv[i] @ g[i] + hess_inv[i - 1] @ g[i - 1]
            ti[i] = hess_inv[i] @ t[i]
            ti_minus[i] = hess_inv[i - 1] @ t[i - 1]

        for k in range(self.options['maxit']):
            d = np.zeros(n)
            jacobian = np.zeros((n, n))

            for i in range(1, n):
                dvec = qi[i] + a[i] * ti[i] - a[i - 1] * ti_minus[i]
                d[i] = np.linalg.norm(dvec)
                for j in range(1, n - 1):
                    if i == j:
                        jacobian[
                            i,
                            j] = 2.0 * qi[i] @ ti[i] + 2.0 * a[i] * ti[i] @ ti[i] - 2.0 * a[i - 1] * ti[i] @ ti_minus[i]
                    elif i - 1 == j:
                        jacobian[i, j] = -2.0 * a[i] * ti[i] @ ti_minus[i] - 2.0 * qi[i] @ ti_minus[i] + 2.0 * a[
                            i - 1] * ti_minus[i] @ ti_minus[i]
                    jacobian[i, j] /= (2.0 * d[i])

            total_length = np.sum(d)
            residual = [di - 1.0 / (n-1.0) * total_length for di in d]
            residual[0] = 0.0

            u, s, vh = np.linalg.svd(jacobian)
            for i, sv in enumerate(s):
                if abs(sv) > 1e-4:
                    s[i] = 1.0 / sv
            jinv = vh.T @ (np.diag(s) @ u.T)
            delta_a = -jinv @ residual

            a += delta_a

            if np.linalg.norm(residual) < self.options['maxrms']:
                break

        delta_x = [-(hess_inv[i] @ g[i]) + a[i] * hess_inv[i] @ t[i] for i in range(n)]
        coords = [(x[i] + delta_x[i]).reshape(-1, 3) for i in range(n)]

        mol_list = []
        for i in range(n):
            mol = replace(molecules[i], coordinates=coords[i])
            mol_list.append(mol)

        return mol_list

    def _get_projected_gradients(self, molecules, tangents):
        """Compute projected gradients."""
        pgrad = np.zeros_like(tangents)
        grad = [mol.properties.get('gradient', np.zeros(len(tangents[0]))).ravel() for mol in molecules]
        for i, gi in enumerate(grad):
            tmat = np.identity(len(tangents[i])) - np.outer(tangents[i], tangents[i])
            pgrad[i] = tmat @ gi
        return pgrad

    def optimize(self, num: Optional[int] = 0):
        """Optimize reaction path.

        Parameters
        ----------
        num : int
            total number of molecules along the path

        """
        if self.options['align']:
            self.molecules = self.align_molecules()
            print('{len(self.molecules)-1} molecules aligned.')

        if num > len(self.molecules):
            num_intermediates = num - len(self.molecules)
            self.molecules = self.interpolate(num)
            print(f'{num_intermediates} intermediates created.')

        # self.molecules = self.initiate_molecules(self.molecules)

        print('\n Optimizing reaction path:\n')
        for i in range(self.options['maxit']):
            self.molecules = self.update_molecules_hessian(self.molecules)
            self.tangents = self.get_tangents()
            self.molecules = self._optimize_distances(self.molecules, self.tangents)
            projected_gradients = self._get_projected_gradients(self.molecules, self.tangents)
            rms = np.sqrt(np.mean(projected_gradients**2))
            print(f'iteration {i}: {rms}')
            if rms <= self.options['maxrms']:
                break

    def numerical_hessian(self, molecule):
        """Compute numerical hessian and diagonalize."""
        h = 0.05
        coord = molecule.coordinates.ravel()
        n = len(coord)
        hessian = np.zeros((n, n))
        for i in range(n):
            newcoord1 = coord.copy()
            newcoord2 = coord.copy()

            newcoord1[i] += h
            newcoord2[i] -= h

            mol1 = replace(molecule, coordinates=newcoord1.reshape(-1, 3))
            mol2 = replace(molecule, coordinates=newcoord2.reshape(-1, 3))

            grad1 = self.method.get_gradient(mol1).ravel()
            grad2 = self.method.get_gradient(mol2).ravel()
            for j in range(n):
                hessian[i, j] = (grad1[j] - grad2[j]) / (2.0*h)

        prop = molecule.properties.copy()
        prop.update({'numerical_hessian': hessian})
        ev, vec = np.linalg.eig(hessian)
        prop.update({'numerical_hessian_eigenvectors': vec, 'numerical_hessian_eigenvalues': ev})
        return replace(molecule, properties=prop)


if __name__ == '__main__':

    import hylleraas as hsp
    myenv = hsp.create_compute_settings('local',
                                        path='/Users/tilmann/Downloads/orca_5_0_3_macosx_intel_openmpi411/')
    mymeth = hsp.Method({'qcmethod': ['DFT', 'BP86']}, program='orca', compute_settings=myenv)

    water = hsp.Molecule('O 0 0 0 \nH 0 0 1 \nH 0 .7 -1')
    geo = hsp.Compute('geometry_optimization', method=mymeth, molecule=water)
    atoms = geo.result['atoms']
    coord0 = np.array(geo.result['final_geometry_bohr'])
    coord1 = np.array([[0, 0, 0], coord0[1] - coord0[0], coord0[2] - coord0[0]])
    coord2 = np.array([coord1[0], -1.0 * coord1[2], -1.0 * coord1[1]])
    water1 = hsp.Molecule({'atoms': ['O', 'H', 'H'], 'coordinates': coord1})
    water2 = hsp.Molecule({'atoms': ['O', 'H', 'H'], 'coordinates': coord2})

    myr = ReactionPath([water1, water2], method=mymeth)

    print('\n Initial molecules:\n', myr.molecules)

    print('\nAliged molecules:\n', myr.align_molecules())

    print('\nInterpolated reaction path:\n', myr.interpolate(5))

    myr.optimize(3)

    print('\nFull reaction path:\n', myr.molecules)

    tst = myr.estimate_tst()
    O1 = tst.coordinates[0]
    H1 = tst.coordinates[1]
    H2 = tst.coordinates[2]
    u = H1 - O1
    v = H2 - O1
    dist1 = np.linalg.norm(u)
    dist2 = np.linalg.norm(v)

    theta = np.arccos(u @ v / (dist1*dist2)) * 180.0 / np.pi
    print('\nestimated tst:', dist1, dist2, theta, tst.properties['energy'])
    # print('numerical Hessian of tst', myr.numerical_hessian(tst))
    myr.plot(reference=0, unit='kJ/mol')
